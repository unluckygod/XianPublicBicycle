//index.js
//获取应用实例
var app = getApp()
Page({
  
  /**
   * 页面的初始数据
   */
  data: {
    longitude: '108.94704937934875',
    latitude: '34.25938010628658',
    scale: '14',
    markers: [],
    inputAddress:'',
    isShowSeachPoint: false,
    includePoints: [],
    buttonSize: 'mini',
    controls: [{
      id: 1,
      iconPath: '../images/location.png',
      position: {
        left: 5,
        top: 385,
        width: 25,
        height: 25
      },
      clickable: true
    }]
  },

  /**
   * 重新定位
   */
  controltap(e) {
    var _this = this;
    _this.setData({
      isShowSeachPoint:false
    });
    this.reloadLocation(_this);
    
  },

  onReady: function (e) {
    // 使用 wx.createMapContext 获取 map 上下文
    //this.mapCtx = wx.createMapContext('myMap')
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this = this;
    this.reloadLocation(_this);
  },

  /**
   * 开始定位获取数据
   */
  reloadLocation: function(_this){
    wx.getLocation({
      success: function (res) {
        _this.setData({
          longitude: res.longitude,
          latitude: res.latitude
        });
        // 更新缓存信息
        _this.synchronizeData();

        // 获取当前位置信息站点信息
        _this.getLocationInfo();
      },
    });
  },

  synchronizeData: function () {
    var url = app.globalData.apiUrl+"synchronize?t=" + new Date().getTime();
    wx.request({
      url: url,
      method: 'post',
      dataType: 'json',
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
      }
    });
  },

  getLocationInfo: function () {
    var _this = this;
    var markers = new Array();
    var includePoints = new Array();
    wx.request({
      url: app.globalData.apiUrl+'getSearch',
      data: {
        lng: this.data.longitude,
        lat: this.data.latitude,
        radius: "1500",
        type:2
      },
      method: 'get',
      dataType: 'json',
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        if (200 == res.statusCode && res.data) {
          for (var i = 0; i < res.data.length; i++) {
            var marker = {
              id: res.data[i].siteid,
              longitude: res.data[i].longitude,
              latitude: res.data[i].latitude,
              iconPath: res.data[i].siteState == 1 ? '../images/zxc_g1.png' : '../images/zxc_r1.png'
            };
            var includePoint = {
              latitude: res.data[i].latitude,
              longitude: res.data[i].longitude
            };
            includePoints.push(includePoint);
            markers.push(marker);
          }

        }
        if (_this.data.isShowSeachPoint) {
          var pointMarker = {
            //id: res.data[i].siteid,
            longitude: _this.data.longitude,
            latitude: _this.data.latitude,
            iconPath: '../images/point.png',
            width: 20,
            height: 20
          };
          markers.push(pointMarker);
        }
      },
      complete: function () {
        _this.setData({
          markers: markers,
          includePoints: includePoints
        });
        // 该方法无效
        // _this.mapCtx.includePoints({
        //   padding: [0],
        //   points: [
        //     _this.data.includePoints[0],
        //     _this.data.includePoints[_this.data.includePoints.length - 1]
        //   ]
        // })
      }
    })

    //wx.hideShareMenu();
  },

  markertap: function (e) {
    if(e.markerId){
      wx.navigateTo({
        url: '/pages/site-detail/site-detail?id=' + e.markerId
      });
    }
  },

  changeSearchAddressText: function(e){
    this.setData({
      inputAddress: e.detail.value
    });
  },

  searchAddress: function(e){
    if (this.data.inputAddress){
      this.getAddressBySearchText(this.data.inputAddress);
    }else{
      wx.showToast({
        title: '请输入查询地址！',
        image: '../images/prompt.png',
        duration: 2000
      });
    }
  },

  getAddressBySearchText: function(searchText){
    var _this = this;
    wx.request({
      url: app.globalData.apiUrl + 'searchAddress',
      data: {
        'address': searchText
      },
      method: 'get',
      dataType: 'json',
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        if (res && res.data && res.data.data && res.data.data.length > 0){
          var first = res.data.data[0];
          if (first.province != '陕西省'){
            wx.showToast({
              title: '已超出查询范围！',
              image: '../images/prompt.png',
              duration: 2000
            });
          }else{//省内查询
            _this.setData({
              longitude: first.location.lng,
              latitude: first.location.lat,
              isShowSeachPoint: true
            });
            _this.getLocationInfo();
          }
        }else{
          wx.showToast({
            title: '未找到指定位置！',
            image: '../images/prompt.png',
            duration: 2000
          });
        }
      },
      complete: function () {
      }

    });

  },

  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },

  onShareAppMessage:function(res){
    return {
      title: app.globalData.appTitle,
      path: '/pages/index/index',
      success: function (res) {
        wx.showToast({
          title: '转发成功',
          icon: 'success',
          duration: 2000
        });
      },
      fail: function (res) {
        wx.showToast({
          title: '转发失败',
          icon: 'cancel',
          duration: 2000
        });
      }
    }
  },

  kindToggle: function (e) {
    var id = e.currentTarget.id, list = this.data.list;
    for (var i = 0, len = list.length; i < len; ++i) {
      if (list[i].id == id) {
        list[i].open = !list[i].open
      } else {
        list[i].open = false
      }
    }
    this.setData({
      list: list
    });
  }







})
